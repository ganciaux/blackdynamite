.. BlackDynamite documentation master file, created by
   sphinx-quickstart on Tue Jan 24 07:50:52 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: https://www.nicepng.com/png/detail/180-1803537_177kib-900x900-black-dynamite-black-dynamite.png
   :width: 200
   :target: https://gitlab.com/ganciaux/blackdynamite
   
BlackDynamite manual
====================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   documentation
   python
   
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
